var linkMap = document.querySelector(".open-map");	
var mapPopup = document.querySelector(".modal-content-map");
var mapClose = mapPopup.querySelector(".modal-content-close");
var overlay = document.querySelector(".modal-overlay");

linkMap.addEventListener("click", function (event) {
	event.preventDefault();
	mapPopup.classList.add("modal-content-show");
	overlay.classList.add("modal-overlay-show");
});

mapClose.addEventListener("click", function(event) {
	event.preventDefault();
	mapPopup.classList.remove("modal-content-show");
	overlay.classList.remove("modal-overlay-show");
});
