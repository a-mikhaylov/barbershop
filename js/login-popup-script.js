var linkLogin = document.querySelector(".login");
var loginPopup = document.querySelector(".modal-content");
var loginClose = loginPopup.querySelector(".modal-content-close");
var overlay = document.querySelector(".modal-overlay");

linkLogin.addEventListener("click", function (event) {
  event.preventDefault();
  loginPopup.classList.add("modal-content-show");
  overlay.classList.add("modal-overlay-show");
});

loginClose.addEventListener("click", function(event) {
  event.preventDefault();
  loginPopup.classList.remove("modal-content-show");
  overlay.classList.remove("modal-overlay-show");
});
